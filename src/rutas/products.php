<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//get all customers
$app->get('/api/products', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `products` WHERE  state_products = 1";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];    
        }

        return $resultado ?: [];
    }, $response);
});

//get products for inputs and coste

$app->get('/api/productsinput/{search}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $search = $request->getAttribute('search');
        $sql =  "SELECT ipro.id_product  AS id, pro.name_product AS producto, pro.sku AS sku, ipro.cost_products AS costo, ipro.quantity_products AS cantidad FROM inputs_details AS ipro
         INNER JOIN products AS pro
         ON ipro.id_product = pro.sku
         WHERE  pro.name_product LIKE '%{$search}%' OR  pro.sku LIKE '%{$search}%' AND pro.state_products = 1";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id']; 
            $resultado[$i]['costo'] = (int)$registro['costo'];  
            $resultado[$i]['cantidad'] = (int)$registro['cantidad'];   
        }

        return $resultado ?: [];
    }, $response);
});

//get customer for dni or full name
$app->get('/api/productsid/{id}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $id = $request->getAttribute('id');
        $sql =  "SELECT * FROM products where id = $id";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});

$app->get('/api/productssku/{sku}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $sku = $request->getAttribute('sku');
        $sql =  "SELECT * FROM products where sku LIKE '%{$sku}%' OR name_product LIKE '%{$sku}%'";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});

//get products only input
$app->get('/api/productsskuadd/{sku}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $sku = $request->getAttribute('sku');
        // $sql =  "SELECT *  FROM products AS pro
        // WHERE pro.id NOT IN (SELECT * FROM inputs_details AS inp WHERE inp.id_product = pro.id)
        //  AND (sku LIKE '%{$sku}%' OR name_product LIKE '%{$sku}%')";
        $sql =  "SELECT * FROM products 
        WHERE sku LIKE '%{$sku}%' AND sku NOT IN (SELECT id_product FROM inputs_details)";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});

//get products input for id

$app->get('/api/productsinpuntid', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        $sku = $request->getAttribute('sku');
        $sql =  "SELECT id FROM products_input order by id desc limit 1";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});

//create new customer
$app->post('/api/products/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
          //throw new Exception('malo');
        $sql = "INSERT INTO products (id, sku, name_product, photo_link, descriptionProd, creation_date) VALUES 
        (NULL,:sku,:name_product,:photo_link,:descriptionProd, :creation_date)";
        $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
         
        $resultado = $dbConexion->executePrepare($sql, $params);
        return $resultado ?: [];
      }, $response);
  });

  $app->post('/api/productsall/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
      $params = $request->getParams(); 
  
     function consultar($sku){
        $sql =  "SELECT * FROM products where sku = $sku";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);       
        return empty($resultado);
     }

      foreach ($params as $key => $value) {
        if (consultar($value['sku'])) {
            print_r('crear');
            $sql = "INSERT INTO products (id, sku, name_product, photo_link, descriptionProd, creation_date, state_products) VALUES 
            (NULL, :sku, :name_product, :photo_link, :descriptionProd, :creation_date, :state_products)";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executePrepare($sql, $value); 
        }else{
            $sql = "UPDATE products SET 
            sku = :sku,
            name_product = :name_product,
            photo_link = :photo_link,
            descriptionProd = :descriptionProd,
            creation_date = :creation_date,
            state_products = :state_products WHERE sku = :sku";
             $dbConexion = new DBConexion(new Conexion());
             $resultado = $dbConexion->executePrepare($sql, $value);
           
        }

      }
    return $resultado ?: [];
    }, $response);
  });

  //update all information for customer
$app->put('/api/products/update', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE products SET 
        sku = :sku,
        name_product = :name_product,
        photo_link = :photo_link,
        creation_date = :creation_date,
        descriptionProd = :descriptionProd WHERE sku = :sku";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
        
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });


//delete customers (change of statecustomer)
 $app->put('/api/products/delete', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE products SET 
          id = :id,
        state_products = :state_products WHERE id = :id";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
        
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

 //update inputs qty and coste of products
 $app->put('/api/productsupdateinput', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE inputs_details SET 
          quantity_products = :cantidad,
          cost_products = :costo WHERE inputs_details.id_product = :id";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
        
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

 //add inputs detail
 $app->post('/api/productsaddinput', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "INSERT INTO inputs_details (id, id_product, id_products_input, quantity_products, cost_products) VALUES 
        (NULL, :id_product, :id_products_input, :cantidad, :costo)";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
        
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

 //add all inputs of products
 $app->post('/api/productsaddinputall', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
      $params = $request->getParams(); 

      function consultar($sku){
        $sql =  "SELECT * FROM inputs_details where id_product = $sku";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return empty($resultado);
     }

      foreach ($params as $key => $value) {
        if (consultar($value['id_product'])) {
            $sql = "INSERT INTO inputs_details (id, id_product, id_products_input, quantity_products, cost_products) VALUES 
            (NULL, :id_product,:id_products_input,:cantidad,:costo)";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executePrepare($sql, $value);
        }else{

            $sql = "UPDATE inputs_details SET 
            quantity_products = :cantidad,
            cost_products = :costo,
            id_product = :id_product,
            id_products_input = :id_products_input WHERE id_product = :id_product";
             $dbConexion = new DBConexion(new Conexion());
             $resultado = $dbConexion->executePrepare($sql, $value);
        }

      }
    return $resultado ?: [];
    }, $response);
  });

$app->delete('/api/products/delete/{dni}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $sql =  "DELETE FROM customers where sku = :sku";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});



?>