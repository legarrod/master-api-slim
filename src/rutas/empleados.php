<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//get all customers
$app->get('/api/empleados', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `empleados`";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];  
        }

        return $resultado ?: [];
    }, $response);
});

$app->get('/api/empleadoexist', function (Request $request, Response $response) {

    return try_catch_wrapper(function()use ($request){
        //throw new Exception('malo');
        $params = $request->getParams();
        $hashVerify = password_hash($params['contrasena'], PASSWORD_DEFAULT, [15]);
        $user = $params['usuario'];
        $sql =  "SELECT id, nombre_empleado, celular_empleado, usuario, contrasena, rol_empleado FROM `empleados` WHERE usuario = '$user'";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        if ($resultado) {
            $position = $resultado[0];
            $hash = password_verify($params['contrasena'], $position['contrasena']);
            if ($hash) {            
                foreach ($resultado as $i=>$registro)  {
                    $resultado[$i]['id'] = (int)$registro['id'];  
                }
              
            return array('id'=>$resultado[0]['id'], 
            'nombre_empleado'=>$resultado[0]['nombre_empleado'], 
            'celular_empleado'=>$resultado[0]['celular_empleado'], 
            'rol_empleado'=>$resultado[0]['rol_empleado'], 
            'usuario'=>$resultado[0]['usuario']);
            }else{
                return 'Contrasena incorrecta';
            }
        }else{
            return 'Usuario no existe';
        }
        
        
    }, $response);
});

//create new customer
$app->post('/api/empleados/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
          //throw new Exception('malo');
          $params = $request->getParams(); 
          function consultar($dni){

            $sql =  "SELECT * FROM empleados WHERE cc_empleado = '$dni'";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executeQuery($sql);
            return empty($resultado);
            }
            function consultarUser($user){
                $sql =  "SELECT * FROM empleados WHERE usuario = '$user'";
                $dbConexion = new DBConexion(new Conexion());
                $resultado = $dbConexion->executeQuery($sql);
                return empty($resultado);
                }

       if (consultar($params['cc_empleado'])) {
            if (consultarUser($params['usuario'])) {
                $hash = password_hash($params['contrasena'], PASSWORD_DEFAULT, [15]);
                $newdata = array('cc_empleado'=>$params['cc_empleado'], 
                                    'nombre_empleado'=>$params['nombre_empleado'], 
                                    'celular_empleado'=>$params['celular_empleado'], 
                                    'direccion_empleado'=>$params['direccion_empleado'], 
                                    'rol_empleado'=>$params['rol_empleado'],
                                    'usuario'=>$params['usuario'],
                                    'contrasena'=>$hash);
                $sql = "INSERT INTO empleados (id, cc_empleado, nombre_empleado, celular_empleado, direccion_empleado, rol_empleado, usuario, contrasena) VALUES 
                (NULL,:cc_empleado, :nombre_empleado,:celular_empleado,:direccion_empleado, :rol_empleado, :usuario, :contrasena)";
                $dbConexion = new DBConexion(new Conexion());
                $resultado = $dbConexion->executePrepare($sql, $newdata);
            }else{
                return 'Usuario ya existe';
            }
            
       }else{
        $params = $request->getParams();
        $hash = password_hash($params['contrasena'], PASSWORD_DEFAULT, [15]);
        $newdata = array('cc_empleado'=>$params['cc_empleado'], 
                            'nombre_empleado'=>$params['nombre_empleado'], 
                            'celular_empleado'=>$params['celular_empleado'], 
                            'direccion_empleado'=>$params['direccion_empleado'], 
                            'rol_empleado'=>$params['rol_empleado'],
                            'usuario'=>$params['usuario'],
                            'contrasena'=>$hash);
        $sql = "UPDATE empleados SET 
        cc_empleado = :cc_empleado,
        nombre_empleado = :nombre_empleado,
        celular_empleado = :celular_empleado,
        direccion_empleado = :direccion_empleado,
        rol_empleado = :rol_empleado,
        usuario = :usuario,
        contrasena = :contrasena WHERE cc_empleado = :cc_empleado";
         $dbConexion = new DBConexion(new Conexion());
         
         $resultado = $dbConexion->executePrepare($sql, $newdata);

       }
         
         //var_dump($newdata);
       
        return $resultado ?: [];
      }, $response);
  });


  //update all information for customer
  $app->delete('/api/empleados/delete/id={id}', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        $id = $request->getAttribute('id');
        //throw new Exception('malo');
        $sql =  "DELETE FROM empleados where cc_empleado = $id";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);

});

?>