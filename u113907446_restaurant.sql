-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:4306
-- Generation Time: Nov 10, 2021 at 03:49 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u113907446_restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `id` int(11) NOT NULL,
  `id_pedido` varchar(5) NOT NULL,
  `sku_plato` varchar(10) DEFAULT NULL,
  `cantidad_plato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`id`, `id_pedido`, `sku_plato`, `cantidad_plato`) VALUES
(24, 'P002', 'AREP001', 1),
(25, 'P002', 'AREP002', 2),
(26, 'P003', 'HANB001', 2),
(27, 'P004', 'AREP001', 1),
(28, 'P004', 'AREP002', 1),
(29, 'P004', 'HANB001', 1),
(30, 'P005', 'AREP001', 2),
(31, 'P005', 'AREP002', 1),
(32, 'P005', 'HANB001', 3),
(33, 'P006', 'AREP001', 2),
(34, 'P006', 'AREP002', 3),
(35, 'P006', 'HANB001', 1);

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `cc_empleado` varchar(15) DEFAULT NULL,
  `nombre_empleado` varchar(30) DEFAULT NULL,
  `celular_empleado` varchar(15) DEFAULT NULL,
  `direccion_empleado` varchar(40) DEFAULT NULL,
  `rol_empleado` int(5) DEFAULT 0,
  `usuario` varchar(15) DEFAULT NULL,
  `contrasena` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `empleados`
--

INSERT INTO `empleados` (`id`, `cc_empleado`, `nombre_empleado`, `celular_empleado`, `direccion_empleado`, `rol_empleado`, `usuario`, `contrasena`) VALUES
(1, '12345', 'Administrador', '-', '-', 1, 'admin', '$2y$10$7jxEqeWp3v2g67DWRIG0b.Y1avcGG.gJSjTeEteQiVdAw.M.ATBJa'),
(2, '12346', 'Cocinero', '-', '-', 3, 'cocina', '$2y$10$/XhY7zIPjcU0VOX.pHXWBO6Fu1dR/fOQBiPsarLO9eYWm5Mr97IFm'),
(3, '12347', 'Mesero', '-', '-', 2, 'mesero', '$2y$10$UXjo5.D6mPIPcCUregBUYeOqeX0BrKLdAO/DeToZI1edoIHUZiB.G');

-- --------------------------------------------------------

--
-- Table structure for table `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `nit_empresa` varchar(15) NOT NULL,
  `nombre_empresa` varchar(20) NOT NULL,
  `direccion_empresa` varchar(30) DEFAULT NULL,
  `telefono_empresa` varchar(15) DEFAULT NULL,
  `logo_empresa` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mesas`
--

CREATE TABLE `mesas` (
  `id` int(11) NOT NULL,
  `numero_mesa` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mesas`
--

INSERT INTO `mesas` (`id`, `numero_mesa`) VALUES
(13, 'DOMIC'),
(1, 'M001'),
(2, 'M002');

-- --------------------------------------------------------

--
-- Table structure for table `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `numero_mesa` varchar(5) DEFAULT NULL,
  `id_pedido` varchar(5) NOT NULL,
  `nombre_empleado` varchar(30) DEFAULT NULL,
  `direccion_pedido` varchar(30) DEFAULT NULL,
  `observacion_pedido` varchar(50) DEFAULT NULL,
  `estado_pedido` varchar(20) DEFAULT 'PENDIENTE',
  `date_created` datetime DEFAULT NULL,
  `totalOrder` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pedidos`
--

INSERT INTO `pedidos` (`id`, `numero_mesa`, `id_pedido`, `nombre_empleado`, `direccion_pedido`, `observacion_pedido`, `estado_pedido`, `date_created`, `totalOrder`) VALUES
(1, 'M001', 'P001', 'Administrador', NULL, 'La arepa con queso sin mantequilla', 'PAGADO', NULL, NULL),
(2, 'M001', 'P002', 'Administrador', NULL, 'La arepa con queso sin mantequilla', 'CANCELADO', NULL, NULL),
(3, 'M002', 'P003', 'Administrador', NULL, 'Una hamburguesa sin cebolla', 'CANCELADO', NULL, NULL),
(4, 'DOMIC', 'P004', 'Administrador', NULL, NULL, 'PREPARACION', NULL, 21000),
(5, 'M001', 'P005', 'Administrador', NULL, NULL, 'PREPARACION', NULL, 49000),
(6, 'DOMIC', 'P006', 'Administrador', 'Calle 20 No 32-85', 'Gaseosa 3 litros y todas las salsas', 'PENDIENTE', NULL, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `platos`
--

CREATE TABLE `platos` (
  `id` int(11) NOT NULL,
  `sku_plato` varchar(10) NOT NULL,
  `nombre_plato` varchar(30) DEFAULT NULL,
  `descripcion_plato` varchar(200) DEFAULT NULL,
  `valor_plato` int(11) DEFAULT 0,
  `foto_plato` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `platos`
--

INSERT INTO `platos` (`id`, `sku_plato`, `nombre_plato`, `descripcion_plato`, `valor_plato`, `foto_plato`) VALUES
(10, 'AREP001', 'Arepa con queso', 'Arepa rellena de queso con mantequilla', 4000, 'http://localhost/resturante-back/public/imagenes/20210627233812.png'),
(11, 'AREP002', 'Arepa con carne', 'Arepa rellena de carne desmechada y con mantequilla', 5000, 'http://localhost/resturante-back/public/imagenes/20210627234020.png'),
(12, 'HANB001', 'Hamburguesa de la casa', 'Hamburguesa con doble carne, porsion de papas  y doble queso', 12000, 'http://localhost/resturante-back/public/imagenes/20210627234221.png');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(10) DEFAULT NULL,
  `name_product` varchar(100) DEFAULT NULL,
  `photo_link` varchar(200) DEFAULT NULL,
  `descriptionProd` varchar(100) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `state_products` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name_product`, `photo_link`, `descriptionProd`, `creation_date`, `state_products`) VALUES
(1, 'HU001', 'Huevos panal', 'https://mercagana.com.co/wp-content/uploads/2020/07/Huevos-super-panal-30-huevos.jpg', '', '2021-07-07 20:56:46', 0),
(2, 'AC001', 'Aceite fino', 'https://cdn1.totalcode.net/la14/product-zoom/es/aceite--soya-girasol-fino-x-500-ml-1.webp', '', '2021-07-07 23:47:19', 0),
(3, 'PAN001', 'Panela trebol', 'https://www.pazalcampo.com/wp-content/uploads/2020/09/Panela-Cuadrada-500-x-500-Front.png', '', '2021-07-08 08:38:59', 0),
(4, '001', '10KG NARANJA', '0', '0', '2021-08-19 10:25:00', 1),
(5, '002', '5KG NARNJA', '0', '', '2021-07-12 12:31:06', 1),
(6, '003', '1/2 BULTO NARANJA', '0', '', '2021-07-12 12:31:21', 1),
(7, '004', 'BULTO NARANJA', '0', '', '2021-07-12 12:33:39', 1),
(8, '005', 'JUGO DE NARANJA X 1LT', '0', '', '2021-07-12 12:34:12', 1),
(9, '006', 'JUGO DE MANDARINA X 1LT', '0', '', '2021-07-12 12:34:56', 1),
(10, '007', '1KG FRESA', '0', '', '2021-07-12 12:35:22', 1),
(11, '008', '1LB ARANDANOS', '0', '', '2021-07-12 12:35:56', 1),
(12, '009', '250G ARANDANOS', '0', '', '2021-07-12 12:36:40', 1),
(13, '007', '1 KG FRESA', '0', '', '2021-07-12 12:46:46', 0),
(14, '008', '1LB ARANDANOS', '0', '', '2021-07-12 12:47:00', 0),
(15, '009', '250G ARANDANOS', '0', '', '2021-07-12 12:47:22', 0),
(16, '010', '1LB CEREZAS', '0', '', '2021-07-12 12:48:18', 1),
(17, '011', 'PIÑA X UND', '0', '', '2021-07-12 12:48:59', 1),
(18, '012', 'PAPAYA X UND', '0', '', '2021-07-12 12:49:50', 1),
(19, '013', 'AGUACATE X UND', '0', '', '2021-07-12 12:50:31', 1),
(20, '014', 'DOCENA GRANADILLA', '0', '', '2021-07-12 12:50:57', 1),
(21, '015', '1/2 DOCENA GRANADILLA', '0', '', '2021-07-12 12:51:21', 1),
(22, '016', 'PLATANO X KG', '0', '', '2021-07-12 12:51:39', 1),
(23, '017', 'LIMON TAHITI X KG', '0', '', '2021-07-12 12:52:21', 1),
(24, '018', 'LIMON MANDARIN X KG', '0', '', '2021-07-12 12:52:44', 1),
(25, '019', '5KG MANDARINA', '0', '', '2021-07-12 12:53:11', 1),
(26, '020', 'HUEVOS CAMPESINOS', '0', '', '2021-07-12 12:53:31', 1),
(27, '021', 'MANGO TOMMY X KG', '0', '', '2021-07-12 12:53:53', 1),
(28, '022', 'BANANO X KG', '0', '', '2021-07-12 12:56:10', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sku_plato` (`sku_plato`),
  ADD KEY `id_pedido` (`id_pedido`);

--
-- Indexes for table `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nombre_empleado` (`nombre_empleado`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mesas`
--
ALTER TABLE `mesas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numero_mesa` (`numero_mesa`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numero_mesa` (`numero_mesa`),
  ADD KEY `nombre_empleado` (`nombre_empleado`),
  ADD KEY `id_pedido` (`id_pedido`);

--
-- Indexes for table `platos`
--
ALTER TABLE `platos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sku_plato` (`sku_plato`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sku` (`sku`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mesas`
--
ALTER TABLE `mesas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `platos`
--
ALTER TABLE `platos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD CONSTRAINT `detalle_pedido_ibfk_1` FOREIGN KEY (`sku_plato`) REFERENCES `platos` (`sku_plato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detalle_pedido_ibfk_2` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id_pedido`);

--
-- Constraints for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`numero_mesa`) REFERENCES `mesas` (`numero_mesa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pedidos_ibfk_2` FOREIGN KEY (`nombre_empleado`) REFERENCES `empleados` (`nombre_empleado`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
