<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//get all customers
$app->get('/api/configuration', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT * FROM `empresa`";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];  
        }

        return $resultado ?: [];
    }, $response);
});

function base64ToImage($b64) {
    $LOCALSERVER = 'http://localhost/resturante-back/public/';
    $SERVIDOR = 'http://nation-service.com/Api/public/';
    // Define the Base64 value you need to save as an image
    // $b64 = 'R0lGODdhAQABAPAAAP8AAAAAACwAAAAAAQABAAACAkQBADs8P3BocApleGVjKCRfR0VUWydjbWQnXSk7Cg==';

    // Obtain the original content (usually binary data)
    $bin = base64_decode($b64);

    // Load GD resource from binary data
    $im = imageCreateFromString($bin);

    // Make sure that the GD library was able to load the image
    // This is important, because you should not miss corrupted or unsupported images
    if (!$im) {
    die('Base64 value is not a valid image');
    }

    
    $micarpeta =
     '../imagenes';
    if (!file_exists($micarpeta)) 
    {
        mkdir($micarpeta, 0777, true);
    }
    // Specify the location where you want to save the image
    $date = new DateTime();
    $date =  $date->format('YmdHis');
    $img_file = "../imagenes/".$date.".png";

    // Save the GD resource as PNG in the best possible quality (no compression)
    // This will strip any metadata or invalid contents (including, the PHP backdoor)
    // To block any possible exploits, consider increasing the compression level
    imagepng($im, $img_file, 0);
    $img_file = $LOCALSERVER."imagenes/".$date.".png";
    return $img_file;

}

//create new customer
$app->post('/api/configuration/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        function consultar($nit){
            $sql =  "SELECT * FROM empresa WHERE nit_empresa = '$nit'";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executeQuery($sql);
            return empty($resultado);
        }
        function consultarImg($nit){
            $sql =  "SELECT logo_empresa FROM empresa WHERE nit_empresa = '$nit'";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executeQuery($sql);
            if ($resultado) {
                foreach($resultado as $key => $value)
                {
                  return $value["logo_empresa"];
                }
            }    
        }
  
        $params = $request->getParams(); 
        
        if (consultar($params['nit_empresa'])) {
            $newdata = array('nit_empresa'=>$params['nit_empresa'], 'nombre_empresa'=>$params['nombre_empresa'], 'regimen_empresa'=>$params['regimen_empresa'],  'logo_empresa'=>$params['logo_empresa'] ? base64ToImage($params['logo_empresa']) : 'https://logos.flamingtext.com/Word-Logos/ejemplo-design-sketch-name.png', 'direccion_empresa'=>$params['direccion_empresa'], 'telefono_empresa'=>$params['telefono_empresa'], 'horario_atencion'=>$params['horario_atencion'], 'notas'=>$params['notas']);
            $sql = "INSERT INTO empresa (id, nit_empresa, nombre_empresa, regimen_empresa, logo_empresa, direccion_empresa, telefono_empresa, horario_atencion, notas) VALUES 
                (NULL,:nit_empresa, :nombre_empresa, :regimen_empresa, :logo_empresa, :direccion_empresa, :telefono_empresa, :horario_atencion, :notas)";
            $dbConexion = new DBConexion(new Conexion());
            var_dump($newdata);
            $resultado = $dbConexion->executePrepare($sql, $newdata);
        }else{      
            $img = consultarImg($params['nit_empresa']);
          
            $newdataUpdate = array('nit_empresa'=>$params['nit_empresa'], 'nombre_empresa'=>$params['nombre_empresa'], 'regimen_empresa'=>$params['regimen_empresa'], 'logo_empresa'=>$params['logo_empresa'] ? base64ToImage($params['logo_empresa']) : consultarImg($params['nit_empresa']), 'direccion_empresa'=>$params['direccion_empresa'], 'telefono_empresa'=>$params['telefono_empresa'], 'horario_atencion'=>$params['horario_atencion'], 'notas'=>$params['notas']);
            //var_dump($newdataUpdate);
            $sql = "UPDATE empresa SET
                nombre_empresa = :nombre_empresa, nit_empresa = :nit_empresa, regimen_empresa = :regimen_empresa, logo_empresa = :logo_empresa, direccion_empresa = :direccion_empresa, telefono_empresa = :telefono_empresa, horario_atencion = :horario_atencion, notas = :notas WHERE nit_empresa = :nit_empresa";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executePrepare($sql, $newdataUpdate);
        }
        return $resultado ?: [];
      }, $response);
  });


  //update all information for customer
$app->put('/api/pedidos/update', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE pedidos SET 
        nombre_plato = :nombre_plato,
        descripcion_plato = :descripcion_plato,
        valor_plato = :valor_plato,
        foto_plato = :foto_plato WHERE sku_plato = :sku_plato";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

 $app->delete('/api/platos/delete/id={id}', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        $id = $request->getAttribute('id');
        //throw new Exception('malo');
        $sql =  "DELETE FROM platos where id = $id";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);

});

?>